# update_decryptor

This is just a docker image that you can build on your own. You must provide the files yourself, we can't provide them to you. But its got a nice UI, so thats something I guess

## How to build the image?

Go to the folder of this project and run on the terminal
```
docker build -t gen5wdecryptor -f ./dockerfile ./
```

## How to decrypt a update?

Go to the folder where you update is located and run the following: 
```
docker run -it -v $PWD:/mnt gen5wdecryptor
```

It will create a folder named `decrypted` and it will contain the decrypted files. Whatever is not encrypted on the original package will be just an empty file on the decrypted folder. Only decrypted files have content.
## How does it look?

- ![decrypt process](images/decrypt_process.jpg)
